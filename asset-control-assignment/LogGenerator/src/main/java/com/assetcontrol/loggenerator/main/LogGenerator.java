package com.assetcontrol.loggenerator.main;

import org.apache.log4j.Logger;

public class LogGenerator {

	public static void main(String[] args) throws InterruptedException {
		int NUMBER = 100;
		Logger logger =Logger.getLogger(LogGenerator.class);
		int i=1;
		while(true) {
			logger.info("Application started");
			logger.info("Checking all webservice conection ");
			logger.info("Checking all database conection ");
			for(i=1;i<NUMBER;i++) {
				logger.info("Pprocessing block:"+"["+i+"]");
				if(i==50) {
					logger.warn(" 50 %  block  processed");
				}
				if(i==99) {
					logger.error("Error while processing block number "+"["+i+"]");
					logger.info(" Application ended");
					logger.info("Restarting application");
					i=1;
					break;
				}
				Thread.sleep(3000);
				
			}
		}

	}

}
